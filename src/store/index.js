import Vue from "vue"
import Vuex from "vuex"
import axios from "../axios"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: "",
        token: localStorage.getItem("token") || "",
        user: {}
    },
    mutations: {
        auth_request(state) {
            state.status = "loading"
        },
        auth_success(state, data) {
            state.status = "success"
            state.user = data.user
            state.token = data.token

            localStorage.setItem("token", state.token)
            axios.defaults.headers.common["Authorization"] = `Bearer` + state.token
        },
        auth_error(state) {
            state.status = "error"
        },
        logout(state) {
            state.status = ""
            state.token = ""
        },
    },
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request")
                axios.post("user/login", user)
                    .then(resp => {
                        commit("auth_success", resp.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit("auth_error")
                        localStorage.removeItem("token")
                        reject(err)
                    })
            })
        },
        logout({ commit }) {
            return new Promise((resolve) => {
                commit("logout")
                localStorage.removeItem("token")
                delete axios.defaults.headers.common["Authorization"]
                resolve()
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        userData: state => state.user
    }
})