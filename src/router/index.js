import VueRouter from 'vue-router'
import Store from '../store'

const routes = [{
        path: "/",
        component: () =>
            import ("../components/layouts/Theme"),
        meta: { requiresAuth: true },
        children: [{
            name: "my-settings",
            path: "my-settings",
            component: () =>
                import ("../components/my-settings"),
        }, {
            path: "staff",
            component: () =>
                import ("../components/staff"),
            children: [{
                name: "staff.index",
                path: "",
                component: () =>
                    import ("../components/staff/calender")
            }, {
                name: "staff.employees",
                path: "employees",
                component: () =>
                    import ("../components/staff/employees/"),
                children: [{
                    name: "staff.employees.add",
                    path: "add",
                    component: () =>
                        import ("../components/staff/employees/details"),
                }, {
                    name: "staff.employees.edit",
                    path: "edit/:id",
                    component: () =>
                        import ("../components/staff/employees/details"),
                }]
            }]
        }, {
            name: "setup.index",
            path: "/setup",
            component: () =>
                import ("../components/setup/"),
            children: [{
                name: "setup.account-settings",
                path: "account-settings",
                component: () =>
                    import ("../components/setup/account-settings")
            }, {
                name: "setup.locations",
                path: "locations",
                component: () =>
                    import ("../components/setup/locations"),
                children: [{
                    name: "setup.locations.add",
                    path: "add",
                    component: () =>
                        import ("../components/setup/locations/step1")
                }, {
                    name: "setup.locations.step1",
                    path: "step1/:id",
                    component: () =>
                        import ("../components/setup/locations/step1")
                }, {
                    name: "setup.locations.step2",
                    path: "step2/:id",
                    component: () =>
                        import ("../components/setup/locations/step2")
                }, {
                    name: "setup.locations.view",
                    path: ":id",
                    component: () =>
                        import ("../components/setup/locations/view")
                }]
            }, {
                name: "setup.taxes",
                path: "taxes",
                props: true,
                component: () =>
                    import ("../components/setup/taxes"),
                children: [{
                    name: "setup.taxes.add",
                    path: "tax/add",
                    props: true,
                    component: () =>
                        import ("../components/setup/taxes/tax"),
                    meta: { modal: true }
                }, {
                    name: "setup.taxes.edit",
                    path: "tax/edit/:id",
                    props: true,
                    component: () =>
                        import ("../components/setup/taxes/tax"),
                    meta: { modal: true }
                }, {
                    name: "setup.taxes.delete",
                    path: "tax/delete/:id",
                    props: true,
                    component: () =>
                        import ("../components/setup/taxes/delete"),
                    meta: { modal: true },
                    beforeEnter: (to, from, next) => {
                        if (to.params.title) {
                            next()
                        } else {
                            next({ name: "setup.taxes" })
                        }
                    }
                }, {
                    name: "setup.groups.add",
                    path: "tax-groups/add",
                    props: true,
                    component: () =>
                        import ("../components/setup/taxes/group"),
                    meta: { modal: true }
                }, {
                    name: "setup.groups.edit",
                    path: "tax-groups/edit/:id",
                    props: true,
                    component: () =>
                        import ("../components/setup/taxes/group"),
                    meta: { modal: true }
                }]
            }, {
                name: "setup.discount-types",
                path: "discount-types",
                props: true,
                component: () =>
                    import ("../components/setup/discount-types"),
                children: [{
                    name: "setup.discount-types.add",
                    path: "add",
                    props: true,
                    component: () =>
                        import ("../components/setup/discount-types/form"),
                    meta: { modal: true }
                }, {
                    name: "setup.discount-types.edit",
                    path: "edit/:id",
                    props: true,
                    component: () =>
                        import ("../components/setup/discount-types/form"),
                    meta: { modal: true }
                }, {
                    name: "setup.discount-types.delete",
                    path: "delete/:id",
                    props: true,
                    component: () =>
                        import ("../components/setup/discount-types/delete"),
                    meta: { modal: true },
                    beforeEnter: (to, from, next) => {
                        if (to.params.title) {
                            next()
                        } else {
                            next({ name: "setup.discount-types" })
                        }
                    }
                }]
            }]
        }, {
            name: "services",
            path: "services",
            component: () =>
                import ("../components/services/"),
            children: [{
                name: "services.category.add",
                path: "categories/add",
                component: () =>
                    import ("../components/services/categories/"),
                props: true,
                meta: { modal: true }
            }, {
                name: "services.category.edit",
                path: "categories/edit/:id",
                component: () =>
                    import ("../components/services/categories/"),
                props: true,
                meta: { modal: true }
            }, {
                name: "services.category.delete",
                path: "categories/delete/:id",
                component: () =>
                    import ("../components/services/categories/delete"),
                props: true,
                meta: { modal: true },
                beforeEnter: (to, from, next) => {
                    if (to.params.title) {
                        next()
                    } else {
                        next({ name: "services" })
                    }
                }
            }, {
                name: "services.appointment-type",
                path: "appointment-type",
                component: () =>
                    import ("../components/services/appointment-type")
            }, {
                name: "services.service.add",
                path: "service",
                component: () =>
                    import ("../components/services/service")
            }, {
                name: "services.service.edit",
                path: "service/edit/:id",
                component: () =>
                    import ("../components/services/service")
            }, {
                name: "services.package.add",
                path: "package",
                component: () =>
                    import ("../components/services/service")
            }]
        }]
    },
    {
        path: "/auth",
        component: () =>
            import ("../components/layouts/Auth"),
        children: [{
            name: "auth.login",
            path: "login",
            component: () =>
                import ("../components/auth/Login")
        }, {
            name: "auth.forgot-password",
            path: "forgot-password",
            component: () =>
                import ("../components/auth/ForgotPassword")
        }, {
            name: "auth.reset-password",
            path: "reset-password",
            component: () =>
                import ("../components/auth/ResetPassword")
        }, {
            name: "auth.signup",
            path: "signup",
            component: () =>
                import ("../components/auth/Signup")
        }]
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (Store.getters.isLoggedIn) {
            next()
        } else {
            next({ name: "auth.login", query: { status: 3 } })
        }
    } else {
        if (Store.getters.isLoggedIn) {
            next("/")
        } else {
            next()
        }
    }
})

export default router