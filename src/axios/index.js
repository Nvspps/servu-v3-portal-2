import axios from "axios"
import store from "../store"
import router from "../router"

const instance = axios.create({
    baseURL: `http://localhost:8000/portal-api/`
})

const token = localStorage.getItem("token")

if (token) {
    instance.defaults.headers.common["Authorization"] = `Bearer` + token
}

instance.interceptors.response.use(
    response => response,
    error => {
        if (error.response.status === 401) {
            store.dispatch("logout")
            router.push({ name: "auth.login", query: { status: 3 } })
        }

        return Promise.reject(error)
    }
)

export default instance