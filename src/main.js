import Vue from 'vue'
import VueRouter from 'vue-router'
import VueStore from './store'
import router from './router'

import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'

import App from './App.vue'
import Axios from './axios'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VueToast, { position: 'top' })
Vue.use(VueStore)

Vue.component("form-input", () =>
    import ("./components/helpers/form-input"))
Vue.component("form-dropdown", () =>
    import ("./components/helpers/form-dropdown"))
Vue.component("form-button", () =>
    import ("./components/helpers/form-button"))
Vue.component("modal", () =>
    import ("./components/helpers/modal"))
Vue.component("modal-wrapper", () =>
    import ("./components/helpers/modal-wrapper"))

Vue.prototype.$http = Axios;
Vue.prototype.$store = VueStore;

Vue.prototype.appendErrors = (error, form) => {
    if (error.response) {
        let response = error.response.data

        for (let data in response) {
            if (form[data]) {
                if (typeof response[data] === "string") {
                    form[data].error = response[data]
                } else if (typeof response[data] === "object") {
                    form[data].error = response[data][0]
                }
            }
        }
    }
}

Vue.prototype.isEmpty = (str) => {
    return /^\s*$/.test(str) || /^null$/.test(str)
}

Vue.prototype.isValidEmail = (str) => {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(str)
}

Vue.prototype.isValidUrl = (str) => {
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str)
}

Vue.prototype.REQUIRED_MESSAGE = "This field is required."
Vue.prototype.INVALID_EMAIL_MESSAGE = "This is not a proper email format."
Vue.prototype.INVALID_URL_MESSAGE = "This is not a proper url format."

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')